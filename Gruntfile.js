module.exports = function (grunt){
	grunt.initConfig({
		sass: {
    dist: {
      files: [{
        'main.css': 'main.scss',
         expand: true,
         cwd: 'styles',
         src: ['*.scss'],
         dest: '../public',
         ext: '.css'
      }]
  

    watch: {
    	files: ['css/*.scss']
    	tasks: ['css']
    },

    
  }
});

grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-sass');
grunt.registerTask('css', ['sass']);

};